/*
 * Shows usage of
 * literal number separator
 * const auto
 * simple function
*/
#include <iostream>

const auto oneHundredThousand {100'000};
const auto greeting {"hello\n"};

int doubleIt(int x) {return 2 * x;}

int main() {
  std::cout << greeting;
  std::cout << doubleIt(oneHundredThousand) << std::endl;
}
