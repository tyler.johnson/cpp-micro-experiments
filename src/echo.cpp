#include <iostream>
#include <unistd.h>

using std::string;
using std::cin;
using std::cout;
using std::endl;

string getInput();

int main() {
  string ping = getInput();
  cout << ping << endl;
}

string getInput() {
  bool isRunningInConsole = isatty(fileno(stdin)) == 1;
  string ping;

  if (isRunningInConsole) {
    cout << "Input please: ";
    cin >> ping;
  } else {
    ping = "fake inputs";
  }

  return ping;
};
