/*
 * Demonstrates usage of variant whereby a struct contains a 'num' field
 * that is a double in one instance and an integrer in another instance
 *
 * 'holds_alternative' is used to inspect the variant type and branch 
 * accordingly.
*/
#include <iostream>
#include <string>
#include <typeinfo>
#include <variant>

using std::cout;
using std::variant;
using std::holds_alternative;
using std::get;
using std::endl;

struct ThingWithVariableInternalType {
  variant<int, double> num;
};

void outputThing(ThingWithVariableInternalType thing);

int main() {
  ThingWithVariableInternalType intThing;
  ThingWithVariableInternalType doubleThing;

  intThing.num = 5;
  doubleThing.num = 5.5;

  outputThing(intThing);
  outputThing(doubleThing);
}

void outputThing(ThingWithVariableInternalType thing) {
  if (holds_alternative<int> (thing.num)) {
    cout << "int " << get<int>(thing.num) << endl;
  } else {
    cout << "double " << get<double>(thing.num) << endl;
  }
}
