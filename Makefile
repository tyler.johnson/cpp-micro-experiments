compileOptions = -Wall -std=c++2a -O3
releaseDir = release
srcDir = src

srcs := $(wildcard $(srcDir)/*.cpp)
bins := $(patsubst $(srcDir)/%.cpp,$(releaseDir)/%,$(srcs))

.PHONY: all
all: $(bins)

$(releaseDir)/%:: $(srcDir)/%.cpp
	clang-tidy -checks=* $? -- $(compileOptions) 2>/dev/null
	$(CXX) $(compileOptions) $? -o $@

$(bins): | $(releaseDir)

$(releaseDir):
	mkdir $(releaseDir)

.PHONY: clean
clean:
	rm -rf $(releaseDir)

print-% : ; @echo $* = $($*)

.PHONY: runall
runall: $(bins)
	$(patsubst %,%;,$?)
